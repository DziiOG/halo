const functions = require("firebase-functions");
const admin = require("firebase-admin");
const app = require("express")();
const firebase = require("firebase");
admin.initializeApp();

const firebaseConfig = {
	apiKey: "AIzaSyA_GDwIbJTMm292t0IGmsGN_g4bHWD6hFQ",
	authDomain: "halo-84fb8.firebaseapp.com",
	databaseURL: "https://halo-84fb8.firebaseio.com",
	projectId: "halo-84fb8",
	storageBucket: "halo-84fb8.appspot.com",
	messagingSenderId: "849816270474",
	appId: "1:849816270474:web:c00c11adf985802f3a1f73",
	measurementId: "G-X2Z4X1LN70",
};

firebase.initializeApp(firebaseConfig);

const db = admin.firestore();
const FBAuth = (req, res, next) => {
	if (
		req.headers.authorization &&
		req.headers.authorization.startsWith("Bearer ")
	) {
		idToken = req.headers.authorization.split("Bearer ")[1];
	} else {
		console.error("no token found");
		return res.status(403).json({ error: "unauthorized" });
	}

	admin
		.auth()
		.verifyIdToken(idToken)
		.then((decodedToken) => {
			req.user = decodedToken;
			return db
				.collection("users")
				.where("userId", "==", req.user.uid)
				.limit(1)
				.get();
		})
		.then((data) => {
			req.user.handle = data.docs[0].data().handle;
			return next();
		})
		.catch((err) => {
			console.error("Error while verifying token", err);
			return res.status(403).json(err);
		});
};

// get all posts
app.get("/posts", (req, res) => {
	db.collection("posts")
		.orderBy("createdAt", "desc")
		.get()
		.then((data) => {
			let posts = [];
			data.forEach((doc) => {
				posts.push({
					postId: doc.id,
					body: doc.data().body,
					username: doc.data().username,
					createdAt: doc.data().createdAt,
				});
			});
			return res.json(posts);
		})
		.catch((err) => console.error(err));
});

//Create a post
app.post("/post", (req, res) => {
	const newPost = {
		body: req.body.body,
		username: req.body.username,
		createdAt: new Date().toISOString(),
	};

	db.collection("posts")
		.add(newPost)
		.then((doc) => {
			res.json({ message: `document ${doc.id} created succesfully` });
		})
		.catch((err) => {
			res.status(500).json({ error: "something went wrong" });
			console.error(err);
		});
});

//Helper Functions
const isEmail = (email) => {
	const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (email.match(regEx)) return true;
	else return false;
};

const isEmpty = (string) => {
	if (string.trim() === "") return true;
	else return false;
};

//SignUp route
app.post("/signup", (req, res) => {
	const newUser = {
		email: req.body.email,
		password: req.body.password,
		username: req.body.username,
	};

	let errors = {};
	if (isEmpty(newUser.email)) {
		errors.email = "Email must not be empty";
	} else if (!isEmail(newUser.email)) {
		errors.email = "Must be a valid email address";
	}

	if (isEmpty(newUser.password)) {
		errors.password = "Must not be empty";
	}
	if (isEmpty(newUser.username)) {
		errors.username = "Must not be empty";
	}

	if (Object.keys(errors).length > 0) {
		return res.status(400).json(errors);
	}

	const noImg = "no-img.png";

	//Validate sign up
	let token, userId, imageUrl;

	db.doc(`/users/${newUser.username}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				return res
					.status(400)
					.json({ username: " this username is already taken " });
			} else {
				return firebase
					.auth()
					.createUserWithEmailAndPassword(
						newUser.email,
						newUser.password
					);
			}
		})
		.then((data) => {
			userId = data.user.uid;

			return data.user.getIdToken();
		})
		.then((idToken) => {
			token = idToken;
			const userCredentials = {
				username: newUser.username,
				email: newUser.email,
				createdAt: new Date().toISOString(),
				imageUrl: `https://firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${noImg}?alt=media`,
				userId,
				Trips: [],
			};
			imageUrl = userCredentials.imageUrl;

			return db.doc(`/users/${newUser.username}`).set(userCredentials);
			//return true
		})
		.then(() => {
			return res.status(201).json({ token, userId, imageUrl });
		})

		.catch((err) => {
			console.error(err);
			if (err.code === "auth/email-already-in-use") {
				return res
					.status(400)
					.json({ email: "Email is already in use" });
			} else {
				return res.status(500).json({ error: err.code });
			}
		});
});

app.post("/login", (req, res) => {
	const user = {
		email: req.body.email,
		password: req.body.password,
	};

	//Validations

	let errors = {};

	if (isEmpty(user.email)) {
		errors.email = "Must not be empty";
	} else if (!isEmail(user.email)) {
		errors.email = "Must be a valid email address";
	}

	if (isEmpty(user.password)) {
		errors.password = "Must not be empty";
	}

	if (Object.keys(errors).length > 0)
		return resizeTo.status(400).json(errors);

	let userId;
	firebase
		.auth()
		.signInWithEmailAndPassword(user.email, user.password)
		.then((data) => {
			userId = data.user.uid;
			return data.user.getIdToken();
		})
		.then((token) => {
			return res.json({ token, userId });
		})
		.catch((err) => {
			console.error(err);
			if (err.code === "auth/wrong-password") {
				return res.status(403).json({
					general: "Wrong credentials, please try again",
				});
			} else if (err.code === "auth/user-not-found") {
				return res.status(403).json({
					general:
						"Email did not match any acount or wrong credentials",
				});
			} else {
				return res.status(500).json({ error: err.code });
			}
		});
});

app.post("/user", (req, res) => {
	const UserID = {
		userId: req.body.userId,
	};

	db.collection("users")
		.orderBy("createdAt", "desc")
		.get()
		.then((data) => {
			let users = [];
			data.forEach((doc) => {
				users.push({
					userId: doc.data().userId,
					username: doc.data().username,
				});
			});
			return users;
		})
		.then((array) => {
			array.forEach((element) => {
				if (element.userId == UserID.userId) {
					return res.status(201).json(element.username);
				}
			});
		})
		.catch((err) => {
			console.error(err.code);
			return res.status(403).json({ error: err.code });
		});
});

app.post("/imageurl", (req, res) => {
	const UserID = {
		userId: req.body.userId,
	};

	db.collection("users")
		.orderBy("createdAt", "desc")
		.get()
		.then((data) => {
			let users = [];
			data.forEach((doc) => {
				users.push({
					userId: doc.data().userId,
					imageUrl: doc.data().imageUrl,
				});
			});
			return users;
		})
		.then((array) => {
			array.forEach((element) => {
				if (element.userId == UserID.userId) {
					return res.status(201).json(element.imageUrl);
				}
			});
		})
		.catch((err) => {
			console.error(err.code);
			return res.status(403).json({ error: err.code });
		});
});

app.post("/scooters", (req, res) => {
	const newScooter = {
		InUse: req.body.InUse,
		latitude: req.body.latitude,
		longitude: req.body.longitude,
		scooterId: req.body.scooterId,
	};

	db.doc(`/scooters/${newScooter.scooterId}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				return res
					.status(400)
					.json({ scooterId: " this scooterId is already taken " });
			} else {
				db.doc(`/scooters/${newScooter.scooterId}`)
					.set(newScooter)
					.then((doc) => {
						return res.status(203).json({
							message: `scooter created successfully with ID ${newScooter.scooterId}`,
						});
					});
			}
		})
		.catch((err) => {
			console.error(err);

			return res.status(500).json({ error: err.code });
		});
});

app.get("/scooters", (req, res) => {
	db.collection("scooters")
		.orderBy("scooterId", "desc")
		.get()
		.then((data) => {
			let scooter = [];

			data.forEach((doc) => {
				scooter.push({
					InUse: doc.data().InUse,
					latitude: doc.data().latitude,
					longitude: doc.data().longitude,
					scooterId: doc.data().scooterId,
				});
			});
			return res.status(201).json(scooter);
		})
		.catch((err) => {
			return res.status(400).json({ error: err.code });
		});
});

app.put("/scooter", (req, res) => {
	const newLocation = {
		InUse: req.body.InUse,
		latitude: req.body.latitude,
		longitude: req.body.longitude,
		scooterId: req.body.scooterId,
	};

	db.doc(`/scooters/${newLocation.scooterId}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				db.doc(`/scooters/${newLocation.scooterId}`)
					.set(newLocation)
					.then((doc) => {
						return res.status(203).json({
							message: `scooter with ID ${newLocation.scooterId} details updated`,
						});
					});
			} else {
				return res.status(400).json({
					message:
						"the scooter you are trying to move does not exist",
				});
			}
		})
		.catch((err) => {
			console.error(err);

			return res.status(500).json({ error: err.code });
		});
});

app.post("/user/image", (req, res) => {
	const BusBoy = require("busboy");
	const path = require("path");
	const os = require("os");
	const fs = require("fs");

	const busboy = new BusBoy({ headers: req.headers });

	let imageToBeUploaded = {};
	let imageFileName;

	busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
		console.log(fieldname, file, filename, encoding, mimetype);
		if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
			return res.status(400).json({ error: "Wrong file type submitted" });
		}
		// my.image.png => ['my', 'image', 'png']
		const imageExtension = filename.split(".")[
			filename.split(".").length - 1
		];
		// 32756238461724837.png
		imageFileName = `${Math.round(
			Math.random() * 1000000000000
		).toString()}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);
		imageToBeUploaded = { filepath, mimetype };
		file.pipe(fs.createWriteStream(filepath));
	});
	busboy.on("finish", () => {
		admin
			.storage()
			.bucket()
			.upload(imageToBeUploaded.filepath, {
				resumable: false,
				metadata: {
					metadata: {
						contentType: imageToBeUploaded.mimetype,
					},
				},
			})
			.then(() => {
				const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`;
				return db.doc(`/users/${req.user.handle}`).update({ imageUrl });
			})
			.then(() => {
				return res.json({ message: "image uploaded successfully" });
			})
			.catch((err) => {
				console.error(err);
				return res.status(500).json({ error: "something went wrong" });
			});
	});
	busboy.end(req.rawBody);
});

app.put("/user/trips", (req, res) => {
	const UserID = {
		userId: req.body.userId,
	};

	db.collection("users")
		.orderBy("createdAt", "desc")
		.get()
		.then((data) => {
			let trips = [];
			data.forEach((doc) => {
				trips.push({
					userId: doc.data().userId,
					username: doc.data().username,
					trips: doc.data().Trips,
				});
			});
			return trips;
		})
		.then((array) => {
			array.forEach((element) => {
				if (element.userId == UserID.userId) {
					return res.status(201).json(element.trips);
				}
			});
		})
		.catch((err) => {
			console.error(err.code);
			return res.status(403).json({ error: err.code });
		});
});

app.post("/user/trips", (req, res) => {
	const trips = {
		CompletedTripTime: new Date().toISOString(),
		CompletedTrip: req.body.CompletedTrip,
		confirmedTripTime: new Date().toISOString(),
		desLat: req.body.desLat,
		desLong: req.body.desLong,
		userLat: req.body.userLat,
		userLong: req.body.userLong,
		username: req.body.username,
	};

	db.collection("users")
		.doc(`${trips.username}`)
		.update({
			Trips: admin.firestore.FieldValue.arrayUnion(trips),
		})
		.then((doc) => {
			return res.status(201).json({ message: "Added succesfully" });
		})
		.catch((err) => {
			console.log(err.code);
			return res.status(500).json({ error: err.code });
		});
});

exports.api = functions.https.onRequest(app);
